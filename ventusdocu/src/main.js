import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import { routes } from './routes.js'

export const eventBus = new Vue()

Vue.use(VueRouter)

const router = new VueRouter({
  routes,
  mode: 'history'
})

var nv = new Vue({
  el: '#app',
  router,
  render: h => h(App)
})

nv.mounted()
