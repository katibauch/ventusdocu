import Home from './components/Home.vue'
import Design from './components/design/DesignArea.vue'
import Technic from './components/technic/TechnicArea.vue'
import Content from './components/content/ContentArea.vue'
import Imprint from './components/imprint/ImprintArea.vue'

export const routes = [
  {
    path: '',
    component: Home
  },
  {
    path: '/design',
    component: Design
  },
  {
    path: '/technik',
    component: Technic
  },
  {
    path: '/content',
    component: Content
  },
  {
    path: '/impressum',
    component: Imprint
  }
]
