import Home from './components/Home.vue'
import Design from './components/design/DesignArea.vue'
import Technic from './components/technic/TechnicArea.vue'
import Content from './components/content/ContentArea.vue'
import Imprint from './components/imprint/ImprintArea.vue'

export const routes = [
  {
    path: '/dokumentation',
    component: Home
  },
  {
    path: '/dokumentation/design',
    component: Design
  },
  {
    path: '/dokumentation/technik',
    component: Technic
  },
  {
    path: '/dokumentation/content',
    component: Content
  },
  {
    path: '/dokumentation/impressum',
    component: Imprint
  }
]
